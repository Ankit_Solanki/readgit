package com.tikan.readgit.view.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation
import com.tikan.readgit.R
import com.tikan.readgit.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val controller = Navigation.findNavController(this,R.id.nav_host_fragment)


        binding.bottomLayout.btnSearch.setOnClickListener {
            controller.navigate(R.id.searchFragment)
        }

        binding.bottomLayout.btnBookmark.setOnClickListener {
            controller.navigate(R.id.bookmarkFragment)
        }


    }
}